# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\sergy\Dropbox\APK_parameters\APK_parameters.ui'
#
# Created: Wed Jan 21 22:23:29 2015
#      by: PyQt4 UI code generator 4.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(400, 557)
        self.button_open = QtGui.QPushButton(Form)
        self.button_open.setGeometry(QtCore.QRect(10, 10, 121, 23))
        self.button_open.setObjectName(_fromUtf8("button_open"))
        self.textEdit_captions = QtGui.QTextEdit(Form)
        self.textEdit_captions.setGeometry(QtCore.QRect(10, 160, 380, 181))
        self.textEdit_captions.setReadOnly(True)
        self.textEdit_captions.setObjectName(_fromUtf8("textEdit_captions"))
        self.button_save = QtGui.QPushButton(Form)
        self.button_save.setGeometry(QtCore.QRect(274, 10, 111, 23))
        self.button_save.setObjectName(_fromUtf8("button_save"))
        self.textEdit_permissions = QtGui.QTextEdit(Form)
        self.textEdit_permissions.setGeometry(QtCore.QRect(10, 360, 380, 181))
        self.textEdit_permissions.setReadOnly(True)
        self.textEdit_permissions.setObjectName(_fromUtf8("textEdit_permissions"))
        self.textEdit_pacage_sdk = QtGui.QTextEdit(Form)
        self.textEdit_pacage_sdk.setGeometry(QtCore.QRect(10, 40, 380, 101))
        self.textEdit_pacage_sdk.setReadOnly(True)
        self.textEdit_pacage_sdk.setObjectName(_fromUtf8("textEdit_pacage_sdk"))

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "APK parameters", None))
        self.button_open.setText(_translate("Form", "Select APK", None))
        self.textEdit_captions.setHtml(_translate("Form", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"><br /></p></body></html>", None))
        self.button_save.setText(_translate("Form", "Save info to file", None))
        self.textEdit_permissions.setHtml(_translate("Form", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"><br /></p></body></html>", None))
        self.textEdit_pacage_sdk.setHtml(_translate("Form", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"><br /></p></body></html>", None))

