__author__ = 'sshejko'

import sys, os, re
import time
from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import QSettings
from APK_parameters_ui import *
import subprocess
import platform
import ConfigParser
import io


class MainWindow(QtGui.QMainWindow):


    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        self.ui=Ui_Form()
        self.ui.setupUi(self)
        self.ui.button_open.clicked.connect(self.open)
        self.ui.button_save.clicked.connect(self.save)
        
        conf = ConfigParser.RawConfigParser()
        conf.readfp(io.BytesIO(open('conf.cfg', 'r').read().lower()))
        self.aapt = conf.get('def', 'aapt')
        #if platform.system().lower() == 'windows':
        #   if os.path.exists('aapt'):
        #       if os.path.exists('aapt.exe'):
        #           self.aapt = os.path.abspath('aapt.exe')
        #       else:
        #           self.aapt = os.path.abspath('aapt') + '.exe'
        #   else:
        #       print('windows aapt not found')
        #       self.ui.textEdit_pacage_sdk.setText('windows aapt not found')
        #else:
        #   if os.path.exists('aapt'):
        #       self.aapt = os.path.abspath('aapt')
        #   else:
        #       print('linux/mac aapt not found')
        #       self.ui.textEdit_pacage_sdk.setText('linux/mac aapt not found')

        print(self.aapt)

    def open(self):
        file_path = str(QtGui.QFileDialog.getOpenFileName(self, 'Open file', '', '*.apk'))
        if file_path == '':
            return 0
        time_modification = time.ctime(os.path.getmtime(file_path))
        print(file_path)
        command = self.aapt + ' d badging ' + file_path + ' | findstr "package"'
        p = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
        out_package, err = p.communicate()
        #print(out_package)


        command = self.aapt + ' d badging ' + file_path + ' | findstr "application-label"'
        p = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
        out_label, err = p.communicate()
        #print(out_label)


        command = self.aapt + ' d badging ' + file_path + ' | findstr "dkVersion"'
        p = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
        out_sdk, err = p.communicate()
        #print(out_sdk)

        #command = 'aapt d badging ' + file_path + ' | findstr "android."'
        command = self.aapt + ' d permissions ' + file_path
        p = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
        out_permissions_features, err = p.communicate()
        out_permissions_features = re.sub(r',', '\r\n', out_permissions_features)
        #print(out_permissions_features)
        print('end')

        self.ui.textEdit_pacage_sdk.setText(file_path + '\r\n----\r\n' + 'Time creation\modification: ' + time_modification + '\r\n----\r\n' + out_package + '\r\n----\r\n' + out_sdk)
        self.ui.textEdit_captions.setText(out_label)
        self.ui.textEdit_permissions.setText(out_permissions_features)



    def save(self):
        file_path = QtGui.QFileDialog.getSaveFileName(self, 'Save file', '', '*.log')
        if file_path == '':
            return 0
        all_text = self.ui.textEdit_pacage_sdk.toPlainText() + '\r\n+++\r\n' + \
                self.ui.textEdit_captions.toPlainText() + '\r\n+++\r\n' + \
                self.ui.textEdit_permissions.toPlainText()
        out_file = open(file_path, 'w')
        out_file.write(all_text)
        out_file.close()

    #http://stackoverflow.com/questions/5506781/pyqt4-application-on-windows-is-crashing-on-exit
    def closeEvent(self, event):
        sys.exit(0)

app = QtGui.QApplication(sys.argv)
w = MainWindow()
w.show()
sys.exit(app.exec_())