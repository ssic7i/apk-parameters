# README #

Application that shows apk parameters.

* Path to file
* File create date
* Package name
* Version
* min, target, max sdkVersion
* Labels(captions)
* Permissions

![sample.jpg](https://bitbucket.org/repo/K6kedj/images/3455600569-sample.jpg)

## How do I get set up? ##

windows binary:

* Download all files from **dist** folder

* Put there ** aapt.exe**

Main python file:

* APK_parameters.pyw

GUI:

* APK_parameters_ui.py

ui file:

* APK_parameters.ui

### Dependencies ###
run exe file:
***aapt***

*Most of these dlls exe-file not using or present by default, but if it doesn't work please check dlls.*
*output py2exe:*

```
#!shell

Your executable(s) also depend on these dlls which are not included,
you may or may not need to distribute them.
Make sure you have the license if you distribute any of them, and
make sure you don't distribute files belonging to the operating system.
OLEAUT32.dll - C:\Windows\system32\OLEAUT32.dll
USER32.dll - C:\Windows\system32\USER32.dll
IMM32.dll - C:\Windows\system32\IMM32.dll
SHELL32.dll - C:\Windows\system32\SHELL32.dll
ole32.dll - C:\Windows\system32\ole32.dll
MSVCP90.dll - C:\Program Files (x86)\Intel\OpenCL SDK\2.0\bin\x86\MSVCP90.dll
WINMM.dll - C:\Windows\system32\WINMM.dll
COMDLG32.dll - C:\Windows\system32\COMDLG32.dll
ADVAPI32.dll - C:\Windows\system32\ADVAPI32.dll
WS2_32.dll - C:\Windows\system32\WS2_32.dll
WINSPOOL.DRV - C:\Windows\system32\WINSPOOL.DRV
GDI32.dll - C:\Windows\system32\GDI32.dll
KERNEL32.dll - C:\Windows\system32\KERNEL32.dll
```

### For build: ###
* Python 2.7
* PyQT4
* SIP
* aapt
## Deployment instructions ##
python setup.py py2exe
## Who do I talk to? ##
Contact:
ssic7i@gmail.com or sergy@sheyko.pp.ua

#License#

The MIT License (MIT)

Copyright (c) 2015 Sergey Sheyko

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.